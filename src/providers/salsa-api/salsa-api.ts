import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

import { Coin } from "../../models/coin";

import { environment } from '../../environment/environment';

const API_URL = environment.dev_api_url;

@Injectable()
export class SalsaApiProvider {

  constructor(public http: Http) { }

  getCoins(): Observable<Coin[]> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let listingsURL = API_URL + 'listings/';

    return this.http.get(listingsURL, options)
      .map(this.parseCoins);
  }

  getCoinMetadata(id: string): Observable<any> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify({"id": id});
    let metadataURL = API_URL + 'get_metadata/';

    return this.http.post(metadataURL, body, options)
      .map(this.parseCoin);
  }

  getSelectedCoinsPrices(selected_coins: any): Observable<any> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify({"selected_coins": selected_coins});
    let coinsPricesURL = API_URL + 'selected_coins_prices/';

    return this.http.post(coinsPricesURL, body, options)
      .map(this.parseCoinsWithPrices);
  }

  private parseCoin(response: Response) {
    let body = response.json();
    if (body.status.error_code == 0) {
      return Object.values(body["data"])[0] || {};
    } else {
      console.log(`ERROR: ${response}`);
      return false;
    }
  }

  private parseCoins(response: Response) {
    let body = response.json();
    if (body.status.error_code == 0) {
      return body.data || {};
    } else {
      console.log(`ERROR: ${response}`);
      return false;
    }
  }

  private parseCoinsWithPrices(response: Response) {
    let body = response.json();
    if (body.status == undefined) {
      for(var key in body){
        if (body.hasOwnProperty(key)) {
          body[key].quote["USD"].price = parseFloat(body[key].quote["USD"].price).toFixed(4);
          body[key].quote["USD"].percent_change_24h = parseFloat(body[key].quote["USD"].percent_change_24h).toFixed(2);
        }
      }
      return body || {};
    } else {
      console.log(`ERROR: ${response}`);
      return false;
    }
  }

}
