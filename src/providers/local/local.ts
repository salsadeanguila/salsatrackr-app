import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';

import { environment } from '../../environment/environment';

@Injectable()
export class LocalProvider {

  constructor(private storage: Storage) { }

  getSavedCoins() {
    return new Promise(resolve => {
      this.storage.get('saved_coins').then((data) => {
        resolve(data);
      });
    })
  }

}
