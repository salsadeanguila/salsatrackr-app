export class Coin {
  id: number;
  name: string;
  symbol: string;
  category: string;
  slug: string;
  logo: string;
  tags: string[];
  urls: object = {
    "website": [],
    "explorer": [],
    "source_code": [],
    "message_board": [],
    "chat": [],
    "announcement": [],
    "reddit": [],
    "twitter": []
  };
  status: any;
}
