import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Coin } from '../../models/coin';
import { SalsaApiProvider } from '../../providers/salsa-api/salsa-api';
import { LocalProvider } from '../../providers/local/local';
import { Storage } from '@ionic/storage';
import { CoinDetailPage } from '../coin-detail/coin-detail';

@IonicPage()
@Component({
  selector: 'page-add-coin',
  templateUrl: 'add-coin.html',
})
export class AddCoinPage {
  search_string: string;
  coins: Coin[] = [];
  coins_catalog: Coin[] = [];
  saved_coins: any;

  constructor(public nav: NavController,
              public navParams: NavParams,
              private salsaApiProvider: SalsaApiProvider,
              private localProvider: LocalProvider,
              private storage: Storage) {
    this.search_string = "";
    this.coins = [];
  }

  ionViewDidLoad() {
    this.getSavedCoins();
    this.getCoinsCatalog();
  }

  addSelectedCoin(selected_coin) {
    let is_saved: boolean = false;
    // Checks if it's already saved
    this.saved_coins.forEach(function(saved_coin, index){
      if (selected_coin.id == saved_coin.id) {
        is_saved = true;
      }
    }.bind(this));
    // If it isn't, then saves it
    if (is_saved == false) {
      this.saved_coins.push({'id': selected_coin.id});
      this.storage.set('saved_coins', this.saved_coins);
    }
    // Redirects to coin detail page
    this.goToCoinDetailView(selected_coin);
  }

  searchCoin() {
    if (this.search_string.length >= 2) {
      this.coins = [];
      let query = this.search_string.toLowerCase();
      // Loops through coins to filter
      this.coins_catalog.forEach(function(coin, i){
        if (coin.name.toLowerCase().indexOf(query) != -1) {
          this.coins.push(coin);
          return;
        }
        if (coin.symbol.toLowerCase().indexOf(query) != -1) {
          this.coins.push(coin);
          return;
        }
      }.bind(this));
    } else {
      this.coins = [];
    }
  }

  getCoinsCatalog() {
    this.salsaApiProvider.getCoins().subscribe(
      result => {
        if (result.length > 0) {
          this.coins_catalog = result;
        }
      }
    );
  }

  getSavedCoins() {
    this.localProvider.getSavedCoins().then(
      result => {
        if (result == null) {
          // No coins saved, this will trigger once
          this.storage.set('saved_coins', []);
        } else {
          // Stores saved coins on array
          this.saved_coins = Object.assign([], result);
        }
      }
    );
  }

  populateCoinsArray() {
    this.coins = Object.assign([], this.coins_catalog);
  }

  goToCoinDetailView(selected_coin) {
    this.nav.push(CoinDetailPage, {coin: selected_coin});
  }

}
