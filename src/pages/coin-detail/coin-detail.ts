import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Coin } from '../../models/coin';
import { SalsaApiProvider } from '../../providers/salsa-api/salsa-api';
import { LocalProvider } from '../../providers/local/local';
import { Storage } from '@ionic/storage';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { WatchlistPage } from '../watchlist/watchlist';

@IonicPage()
@Component({
  selector: 'page-coin-detail',
  templateUrl: 'coin-detail.html',
})
export class CoinDetailPage {
  selected_coin: any;
  loading: boolean = false;
  saved_coins: any;

  constructor(public nav: NavController,
              public alertCtrl: AlertController,
              public navParams: NavParams,
              private salsaApiProvider: SalsaApiProvider,
              private localProvider: LocalProvider,
              private storage: Storage,
              private inAppBrowser: InAppBrowser) {
  }

  ionViewDidLoad() {
    this.selected_coin = this.navParams.get('coin');
    this.getCoinDetail(this.selected_coin.id);
    this.getSavedCoins();
  }

  getCoinDetail(coin_id){
    this.salsaApiProvider.getCoinMetadata(coin_id).subscribe(
      result => {
        this.selected_coin = result;
        this.loading = true;
        console.log(this.selected_coin);
      }
    );
  }

  getSavedCoins() {
    this.saved_coins = this.storage.get('saved_coins').then((val) => {
      this.saved_coins = Object.assign([], val);
    });
  }

  openUrl(url) {
    const browser = this.inAppBrowser.create(url);
  }

  removeCoin(coin_id) {
    let alert = this.alertCtrl.create({
      title: 'Confirm removal',
      message: 'Stop tracking this coin?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            // Nothing
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.saved_coins.forEach(function(saved_coin, index){
              if (saved_coin.id == coin_id) {
                this.saved_coins.splice(index, 1);
              }
            }.bind(this));
            this.storage.set('saved_coins', this.saved_coins);
            this.nav.popToRoot()
          }
        }
      ]
    });
    alert.present();
  }

}
