import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { SalsaApiProvider } from '../../providers/salsa-api/salsa-api';
import { LocalProvider } from '../../providers/local/local';
import { AddCoinPage } from '../add-coin/add-coin';
import { CoinDetailPage } from '../coin-detail/coin-detail';

@IonicPage()
@Component({
  selector: 'page-watchlist',
  templateUrl: 'watchlist.html',
})
export class WatchlistPage {
  saved_coins: any;
  watchlist: any;

  constructor(public nav: NavController,
              public navParams: NavParams,
              private salsaApiProvider: SalsaApiProvider,
              private localProvider: LocalProvider,
              private storage: Storage) { }

  ionViewDidLoad() {

  }

  ionViewDidEnter() {
    this.getSavedCoins();
  }

  getSavedCoins() {
    this.localProvider.getSavedCoins().then(
      result => {
        this.saved_coins = result;
        this.salsaApiProvider.getSelectedCoinsPrices(this.saved_coins)
          .subscribe(
            coins => {
              this.watchlist = coins;
            }
          )
      }
    );
    // this.saved_coins = this.storage.get('saved_coins').then((val) => {
    //   this.saved_coins = Object.assign([], val);
    //   console.log(this.saved_coins);
    // });
  }

  goToAddCoinView() {
    this.nav.push(AddCoinPage);
  }

  goToCoinDetailView(id) {
    this.nav.push(CoinDetailPage, {coin: {id: id}});
  }

}
