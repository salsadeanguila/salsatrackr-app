export const environment = {
  // DEV API URL
  dev_api_url: 'http://localhost:3000/',
  // PROD API URL
  prod_api_url: 'http://salsatrackr-api.herokuapp.com/'
};
