# Salsatrackr APP

##Español
Frontend del proyecto "Salsatrackr". Hecho con la plantilla Ionic 3 Start Theme. Esta aplicación híbrida permite seguir los precios de las criptomonedas
que aparecen en CoinMarketCap. Permite agregar y eliminar criptomonedas a una lista de seguimiento y ver el detalle de cada moneda.
Este proyecto usa directamente un API subida a mi cuenta de Heroku para facilitar la prueba. Puede tardar un poco al abrir. Si se desea usar el API
localmente se necesita cambiar el url del API seleccionada en los archivos: ```./src/providers/salsa-api/salsa-api.ts``` y ```./src/environment/environment.ts```
##English
Frontend of the project "Salsatrackr". Built with Ionic 3 Start Theme template. This hybrid application allows the user to follow the prices of
cryptocurrencies listed on CoinMarketCap. It allows to add and delete cryptos from a watchlist and display the detailed information of a specific
cryptocurrency. This project uses an API uploaded on my Heroku account to facilitate testing. It may take a little to load. If you wish to use a local
instance of the API you can change the API url selected on the files: ```./src/providers/salsa-api/salsa-api.ts``` and ```./src/environment/environment.ts```

##Info

###Ionic 3

###Angular 4.4.4

###Typescript 2.3.4

###Node

###Installation
-Open a terminal window

-Navigate to project's directory

-Run ```npm install```

-Run ```ionic serve```
